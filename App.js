import React from 'react'
import {createStackNavigator, createAppContainer} from 'react-navigation'

import HomeScreen from './Components/HomeScreen'
import InsertUser from './Components/InsertUser'
import ViewUser from './Components/ViewUser';
import UpdateUser from './Components/UpdateUser'
import DeleteUser from './Components/DeleteUser'
import ViewAllUser from './Components/ViewAllUser'

const App = createStackNavigator({
  HomeScreen:{
    screen: HomeScreen,
    navigationOptions:{
      title: 'Tela Inicial',
      headerStyle:{backgroundColor: 'red'},
      headerTintColor: 'blue',
    }
  },
  InsertUser:{
    screen: InsertUser,
    navigationOptions:{
      title: 'Inserir Usuário',
      headerStyle:{backgroundColor: 'blue'},
      headerTintColor: 'red'
    }
  },
  View:{
    screen: ViewUser,
    navigationOptions:{
      title: 'View User',
      headerStyle: {backgroundColor: 'blue'},
      headerTintColor: 'red'
    }
  },
  ViewAll:{
    screen: ViewAllUser,
    navigationOptions:{
      title: 'View All User',
      headerStyle: {backgroundColor: 'blue'},
      headerTintColor: 'red'
    }
  },
  Update:{
    screen: UpdateUser,
    navigationOptions:{
      title: 'Update User',
      headerStyle: {backgroundColor: 'blue'},
      headerTintColor: 'red'
    }
  },
  Delete:{
    screen: DeleteUser,
    navigationOptions:{
      title: 'Delete User',
      headerStyle: {backgroundColor: 'blue'},
      headerTintColor: 'red'
    }
  },
})

export default createAppContainer(App)