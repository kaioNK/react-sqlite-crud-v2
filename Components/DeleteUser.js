import React from 'react'
import {StyleSheet, Dimensions, View, Alert, ScrollView, KeyboardAvoidingView} from 'react-native'
import MyTextInput from './Layout/MyTextInput'
import MyButton from './Layout/MyButton'
import {Divider, SearchBar, Button, Icon} from 'react-native-elements';
//import LineSeparator from './Layout/LineSeparator'
import {openDatabase} from 'react-native-sqlite-storage'

const db = openDatabase({name: 'usuarios_database.db'})
const tableName = 'table_user3'
const {width, height} = Dimensions.get('window') 

export default class DeleteUser extends React.Component{
  state = {
    input_user_id: ''
  }

  searchUser = () => {
    const {input_user_id} = this.state
    console.log(this.state.input_user_id)
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM ' + tableName + ' WHERE user_id = ?',
        [input_user_id],
        (tx, results) => {
          const len = results.rows.length
          console.log('Len: ', len)
          if(len > 0){
            console.log(results.rows.item(0).user_address)
            this.setState({
              user_name: results.rows.item(0).user_name,
              user_phone: results.rows.item(0).user_phone,
              user_address: results.rows.item(0).user_address
            })
          }else{
            alert('Uusário não encontrado!')
            this.setState({
              user_name: '',
              user_phone: '',
              user_address: ''
            })
          }
        }
      )
    })
  }


  deleteUser = () => {
    const {input_user_id} = this.state
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM ' + tableName + ' where user_id=?', [input_user_id], (tx, results) => {
          console.log('Results: ', results.rowsAffected)
          if(results.rowsAffected > 0){
            Alert.alert('Success','Usuário deletado com sucesso!', [{
              text: 'Ok',
              onPress: () => this.props.navigation.navigate('HomeScreen')
            }], {cancelable: false})
          }else{
            alert('Please insert a valid User ID')
          }
        }
      )
    })
  }

  render(){
    const {input_user_id} = this.state
    return(
      <View style={{backgroundColor: 'white', flex: 1}}>
        <ScrollView>
          <KeyboardAvoidingView
            behavior='padding'
            style={{flex:1, justifyContent: 'space-between'}}
          >
            <SearchBar 
              placeholder='Digite o ID do usuário'
              style={{padding:10}}
              onChangeText={input => this.setState({input_user_id: input})}
              value={input_user_id}
            />
            
            <Button 
              title="Busca usuário"
              icon={
                <Icon
                  name='ios-american-football'
                  type='ionicon'
                  size={15}
                  color="white"
                  buttonStyle={{
                    backgroundColor:'red'
                  }}
                />
              }
              iconRight
              onPress={() => this.searchUser()}
            />

            <Divider style={{
              backgroundColor:'blue', 
              margin: 10,
              padding: 2,
              
              }} 
            />

            <MyTextInput 
              placeholder='Nome do usuário'
              value={this.state.user_name}
              editable={false}
            />
            <MyTextInput 
              placeholder='Phone do usuário'
              value={this.state.user_phone}
              editable={false}
            />
            <MyTextInput 
              placeholder='Endereço do usuário'
              value={this.state.user_address}
              editable={false}
            />

            <MyButton 
              title='Delete User'
              customClick={() => this.deleteUser()}
            />

          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  btn:{
    alignItems: 'center',
    padding: 10,
    marginTop: 16,
    marginLeft: 35,
    marginRight: 35,
  }
}) 