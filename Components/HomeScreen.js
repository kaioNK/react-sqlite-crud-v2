import React, {Component} from 'react'
import {View} from 'react-native'
import MyButton from './Layout/MyButton'
import MyText from './Layout/MyText'
import MyTextInput from './Layout/MyTextInput'
import {openDatabase} from 'react-native-sqlite-storage'

let db = openDatabase({name: 'usuarios_database.db'})

class HomeScreen extends Component{
  constructor(props){
    super(props)
    db.transaction(function(txn){
      txn.executeSql(
        "select name from sqlite_master where type='table' and name='table_user3'", [],
        function(tx, res){
          console.log('item: ', res.rows.length)
          if(res.rows.length === 0){
            txn.executeSql('drop table if exists table_user3', [])
            txn.executeSql(
              'create table if not exists table_user3(user_id integer primary key autoincrement, user_name varchar(30), user_phone varchar(10), user_address varchar(255))', []
            )
          }
        }
      )
    })
  }
  render(){
    return(
      <View
        style={{flex:1, backgroundColor: 'white', flexDirection: 'column'}}
      >
        <MyText text='SQLite Example' />
        <MyButton title='Insert' customClick={() => this.props.navigation.navigate('InsertUser')} />
        <MyButton title='Update' customClick={() => this.props.navigation.navigate('Update')} />
        <MyButton title='View User' customClick={() => this.props.navigation.navigate('View')} />
        <MyButton title='View All Users' customClick={() => this.props.navigation.navigate('ViewAll')} />
        <MyButton title='Delete' customClick={() => this.props.navigation.navigate('Delete')} />
      </View>
    )
  }
}

export default HomeScreen