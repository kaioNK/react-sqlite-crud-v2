import React, {Component} from 'react'
import {View, ScrollView, KeyboardAvoidingView, Alert} from 'react-native'
import MyTextInput from './Layout/MyTextInput'
import MyButton from './Layout/MyButton'
import {openDatabase} from 'react-native-sqlite-storage'

let db = openDatabase({name: 'usuarios_database.db'})

class InsertUser extends Component{
  state = {
    user_name: '',
    user_phone: '',
    user_address: ''
  }

  insert_user = () => {
    let that = this //se não tiver isso não funciona!
    const {user_name, user_phone, user_address} = this.state
    if(user_name){
      if(user_phone){
        if(user_address){
          db.transaction(function(tx){
            tx.executeSql(
              'INSERT INTO table_user3 (user_name, user_phone, user_address) VALUES (?,?,?)', [user_name, user_phone, user_address],
              (tx, results) => {
                console.log('Results: ', results.rowsAffected)
                if(results.rowsAffected > 0){
                  Alert.alert('Success', 'You are Registered Successfully', [{
                    text: 'Ok, valeu!!!',
                    onPress: () => that.props.navigation.navigate('HomeScreen')
                  }], {cancelable: false})
                }else{
                  alert('Registration Failed')
                }
              }
            )
          })
        }else{
          alert('Please fill Address')
        }
      }else{
        alert('Please fill Contact Number')
      }
    }else{
      alert('Please fill Name')
    }
  }

  render(){
    return(
      <View style={{backgroundColor: 'white', flex:1}}>
        <ScrollView keyboardShouldPersistTaps='handled'>
          <KeyboardAvoidingView
            behavior='padding'
            style={{flex:1, justifyContent: 'space-between'}}
          >
            <MyTextInput 
              placeholder="Digite o nome"
              onChangeText={user_name => this.setState({user_name})}
              style={{padding: 10}}
            />

            <MyTextInput
              placeholder='Digite o telefone'
              onChangeText={user_phone => this.setState({user_phone})}
              maxLength={10}
              keyboardType='numeric'
              style={{padding:10}}
            />

            <MyTextInput
              placeholder='Digite o endereço'
              onChangeText={user_address => this.setState({user_address})}
              maxLength={255}
              numberOfLines={10}
              multiline={true}
              style={{textAlignVertical: 'top', padding:10}}
            />

            <MyButton 
              title='Adicionar'
              customClick={() => this.insert_user()}
            />
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
}

export default InsertUser