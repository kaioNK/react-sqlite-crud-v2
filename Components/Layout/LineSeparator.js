import React from 'react'
import {View} from 'react-native'

/**
 * Pode ser usado como separador mas é muito feio, 
 * melhor usar o react-native-elements!
 */
const LineSeparator = () => {
  return(
    <View 
      style={{
        borderBottomColor: 'black',
        borderBottomWidth: 1
      }}
    />
  )
}

export default LineSeparator