import React from 'react'
import {View, TextInput, StyleSheet} from 'react-native'

const MyTextInput = ({
  placeholder, 
  keyboardType, 
  onChangeText,
  returnKeyType,
  numberOfLine,
  multiline, 
  onSubmitEditing,
  value,
  editable
}) => {
  return(
    <View>
      <TextInput 
        underlineColorAndroid='transparent'
        placeholder={placeholder}
        placeholderTextColor='#007FFF'
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        returnKeyType={returnKeyType}
        numberOfLines={numberOfLine}
        multiline={multiline}
        onSubmitEditing={onSubmitEditing}
        style={styles.textInput}
        blurOnSubmit={false}
        value={value}
        editable={editable}
      />
    </View>
  )
}

export default MyTextInput

const styles = StyleSheet.create({
  textInput:{
    marginLeft: 35,
    marginRight: 35,
    marginTop: 10,
    borderColor: '#007FFF',
    borderWidth: 1,

  }
})