import React from 'react'
import {View, YellowBox, ScrollView, KeyboardAvoidingView, Alert} from 'react-native'
import MyTextInput from './Layout/MyTextInput'
import MyButton from './Layout/MyButton'
import {openDatabase} from 'react-native-sqlite-storage'

const db = openDatabase({name: 'usuarios_database.db'})
const tableName = 'table_user3'
//const idName = 'user_id'

export default class UpdateUser extends React.Component{
  state = {
    input_user_id: '',
    user_name: '',
    user_phone: '',
    user_address: ''
  }

  searchUser = () => {
    const {input_user_id} = this.state
    console.log(this.state.input_user_id)
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM ' + tableName + ' WHERE user_id = ?',
        [input_user_id],
        (tx, results) => {
          const len = results.rows.length
          console.log('Len: ', len)
          if(len > 0){
            console.log(results.rows.item(0).user_address)
            this.setState({
              user_name: results.rows.item(0).user_name,
              user_phone: results.rows.item(0).user_phone,
              user_address: results.rows.item(0).user_address
            })
          }else{
            alert('No user found')
            this.setState({
              user_name: '',
              user_phone: '',
              user_address: ''
            })
          }
        }
      )
    })
    console.log('user_name state: ', this.state.user_name)
  }

  updateUser = () => {
    console.log('Update!')
    let that = this //se não tiver isso não funciona!
    const {input_user_id, user_name, user_phone, user_address} = this.state
    if(user_name){
      if(user_phone){
        if(user_address){
          db.transaction(tx => {
            tx.executeSql(
              'UPDATE ' + tableName + ' set user_name=?, user_phone=?, user_address=? where user_id=?',
              [user_name, user_phone, user_address, input_user_id],
              (tx, results) => {
                console.log('Results: ', results.rowsAffected)
                if(results.rowsAffected > 0){
                  Alert.alert('Success', 'User updated successfully',
                    [
                      {text: 'Ok', onPress: () => that.props.navigation.navigate('HomeScreen')},
                    ],
                    {cancelable: false}
                  )
                }else{
                  alert('Update failed')
                }
              }
            )
          })
        }else{
          alert('Plase fill address')
        }
      }else{
        alert('Please fill contact number')
      }
    }else{
      alert('Plase fill name')
    }
  }

  render(){
    return(
      <View>
        <ScrollView keyboardShouldPersistTaps='handled'>
          <KeyboardAvoidingView
            behavior='padding'
            style={{flex:1, justifyContent: 'space-between'}}>
            
            <MyTextInput 
              placeholder='Enter user ID'
              style={{padding:10}}
              onChangeText={input => this.setState({input_user_id: input})}
            />
            <MyButton 
              title='Search User'
              customClick={() => this.searchUser()}
            />

            <MyTextInput 
              placeholder='Enter name'
              value={this.state.user_name}
              style={{padding:10}}
              onChangeText={input => this.setState({user_name: input})}
            />

            <MyTextInput 
              placeholder="Enter phone number"
              value={'' + this.state.user_phone}
              onChangeText={input => this.setState({user_phone: input})}
              maxLength={10}
              style={{padding:10}}
              keyboardType='numeric'
            />

            <MyTextInput
              value={this.state.user_address}
              placeholder='Enter address'
              onChangeText={input => this.setState({user_address: input})}
              maxLength={255}
              numberOfLines={5}
              multiline={true}
              style={{textAlignVertical: 'top', padding:10}}
            />

            <MyButton 
              title='Update User'
              customClick={() => this.updateUser()}
            />
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
}