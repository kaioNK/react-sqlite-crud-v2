import React from 'react'
import {FlatList, Text, View} from 'react-native'
import {openDatabase} from 'react-native-sqlite-storage'

const db = openDatabase({name: 'usuarios_database.db'})
const tableName = 'table_user3'

export default class ViewAllUser extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      FlatListItems: []
    }

    db.transaction(tx => {
      tx.executeSql('SELECT * FROM ' + tableName, [], (tx, results) => {
        let temp = []
        for(let i = 0; i < results.rows.length; i++){
          temp.push(results.rows.item(i))
        }
        this.setState({
          FlatListItems: temp
        })
      })
    })
  }

  ListViewItemSeparator = () => {
    return(
      <View style={{height:0.2, width: '100%', backgroundColor: '#808080'}} />
    )
  }
 
  render(){
    return(
      <View>
        <FlatList
          data={this.state.FlatListItems}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <View key={item.user_id} style={{backgroundColor: 'white', padding:20}}>
              <Text>ID: {item.user_id}</Text>
              <Text>Name: {item.user_name}</Text>
              <Text>Phone: {item.user_phone}</Text>
              <Text>Address: {item.user_address}</Text>
            </View>
          )}
        />
      </View>
    )
  }
}