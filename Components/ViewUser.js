import React from 'react'
import {Text, View, Button} from 'react-native'
import MyTextInput from './Layout/MyTextInput'
import MyButton from './Layout/MyButton'
import {openDatabase} from 'react-native-sqlite-storage'

const db = openDatabase({name: 'usuarios_database.db'})
const tableName = 'table_user3'

export default class ViewUser extends React.Component{
  state = {
    input_user_id: '',
    userData: ''
  }

  searchUser = () => {
    const {input_user_id} = this.state
    console.log(this.state.input_user_id)
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM ' + tableName + ' where user_id = ?', [input_user_id],(tx,results) => {
          const len = results.rows.length
          console.log('Len: ', len)
          if(len > 0){
            this.setState({
              userData: results.rows.item(0)
            })
            console.log('results.rows.item(0): ', results.rows.item(0))
          }else{
            alert('No user found')
            this.setState({userData: ''})
          }
        }
      )
    })
  }

  render(){
    return(
      <View>
        <MyTextInput 
          placeholder="Digite a ID do usuário"
          onChangeText={input => this.setState({input_user_id: input})}
          style={{padding:10}}
        />
        <MyButton
          title='Search User'
          customClick={() => this.searchUser()}
        />
        <View style={{marginLeft: 35, marginRight: 35, marginTop: 10}}>
          <Text>User ID: {this.state.userData.user_id}</Text>
          <Text>Name: {this.state.userData.user_name}</Text>
          <Text>Phone: {this.state.userData.user_phone}</Text>
          <Text>Address: {this.state.userData.user_address}</Text>
        </View>
      </View>
    )
  }
}